import configparser
import sqlalchemy
import os

# Get properties for database connectivity and mode from ConfigFile
thisfolder = os.path.dirname(os.path.abspath(__file__))
initfile = os.path.join(thisfolder, 'ConfigFile.properties')
config = configparser.RawConfigParser()
config.read(initfile)
database_username = config.get('DatabaseSection', 'database_username')
database_password = config.get('DatabaseSection', 'database_password')
database_ip = config.get('DatabaseSection', 'database_ip')
database_name = config.get('DatabaseSection', 'database_name')
data_importer_mode = config.get('DatabaseSection', 'database_mode')

# create connection
database_connection = sqlalchemy.create_engine('mysql+pymysql://{0}:{1}@{2}/{3}'.
                                               format(database_username, database_password,
                                                      database_ip, database_name))


# Given the dataframe and the table this functions inserts dataframe values into the selected database table
def insert_dataframe_to_db(R_dataframe, table_name):
    # query data
    if data_importer_mode in ["Create", "CREATE", "create"]:
        R_dataframe.to_sql(con=database_connection, name=table_name, if_exists='replace')
    elif data_importer_mode in ["Update", "UPDATE", "update"]:
        R_dataframe.to_sql(con=database_connection, name=table_name, if_exists='append')
    elif data_importer_mode in ["None", "NONE", "none"]:
        R_dataframe.to_sql(con=database_connection, name=table_name, if_exists='fail')
    else:
        print("Invalid mode property value")

