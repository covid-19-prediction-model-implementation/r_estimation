import numpy as np
import pandas as pd
from IPython.display import clear_output
from scipy import stats as sps
import dbConnection as dbConn
import mysql.connector
import sqlalchemy


# Function declarations
def prepare_cases(cases, cutoff=25):
    new_cases = cases.diff()

    smoothed = new_cases.rolling(7,
                                 win_type='gaussian',
                                 min_periods=1,
                                 center=True).mean(std=2).round()

    smoothedravelled = np.ravel(smoothed)
    idx_start = np.searchsorted(smoothedravelled, cutoff)

    smoothed = smoothed.iloc[idx_start:]
    original = new_cases.loc[smoothed.index]

    return original, smoothed


def get_posteriors(sr, sigma=0.15):
    # (1) Calculate Lambda
    lam = sr[:-1].values.T * np.exp(GAMMA * (r_t_range[:, None] - 1))

    # (2) Calculate each day's likelihood
    r_t_range[1:]
    likelihoods = pd.DataFrame(
        data=sps.poisson.pmf(sr[1:].values.T, lam),
        index=r_t_range,
        columns=sr.index[1:])

    # (3) Create the Gaussian Matrix
    process_matrix = sps.norm(loc=r_t_range,
                              scale=sigma
                              ).pdf(r_t_range[:, None])

    # (3a) Normalize all rows to sum to 1
    process_matrix /= process_matrix.sum(axis=0)

    # (4) Calculate the initial prior
    # prior0 = sps.gamma(a=4).pdf(r_t_range)
    prior0 = np.ones_like(r_t_range) / len(r_t_range)
    prior0 /= prior0.sum()

    # Create a DataFrame that will hold our posteriors for each day
    # Insert our prior as the first posterior.
    posteriors = pd.DataFrame(
        index=r_t_range,
        columns=sr.index,
        data={sr.index[0]: prior0}
    )

    # We said we'd keep track of the sum of the log of the probability
    # of the data for maximum likelihood calculation.
    log_likelihood = 0.0

    # (5) Iteratively apply Bayes' rule
    for previous_day, current_day in zip(sr.index[:-1], sr.index[1:]):
        # (5a) Calculate the new prior
        current_prior = process_matrix @ posteriors[previous_day]

        # (5b) Calculate the numerator of Bayes' Rule: P(k|R_t)P(R_t)
        numerator = likelihoods[current_day] * current_prior

        # (5c) Calcluate the denominator of Bayes' Rule P(k)
        denominator = np.sum(numerator)

        # Execute full Bayes' Rule
        posteriors[current_day] = numerator / denominator

        # Add to the running sum of log likelihoods
        log_likelihood += np.log(denominator)

    return posteriors, log_likelihood


def highest_density_interval(pmf, p=.9, debug=False):
    # If we pass a DataFrame, just call this recursively on the columns
    if isinstance(pmf, pd.DataFrame):
        return pd.DataFrame([highest_density_interval(pmf[col], p=p) for col in pmf],
                            index=pmf.columns)

    cumsum = np.cumsum(pmf.values)

    # N x N matrix of total probability mass for each low, high
    total_p = cumsum - cumsum[:, None]

    # Return all indices with total_p > p
    lows, highs = (total_p > p).nonzero()

    # Find the smallest range (highest density)
    best = (highs - lows).argmin()

    low = pmf.index[lows[best]]
    high = pmf.index[highs[best]]

    return pd.Series([low, high],
                     index=[f'Low_{p * 100:.0f}',
                            f'High_{p * 100:.0f}'])


# End of Function declarations

# These regions will NOT be included in the calculation
FILTERED_REGION_CODES = ['AR', 'AS', 'CB', 'CL', 'CM', 'MD', 'AN', 'IB', 'CN',
                         'CE', 'VC', 'EX', 'GA', 'CT', 'ML',
                         'NC', 'PV', 'RI']

state_name = 'MD'
GAMMA = 1 / 7
k = np.array([20, 40, 55, 90])

# We create an array for every possible value of Rt
R_T_MAX = 12
r_t_range = np.linspace(0, R_T_MAX, R_T_MAX * 100 + 1)
# %config InlineBackend.figure_format = 'retina'

data = pd.read_csv(
    'C:/developmentFiles/agregados.csv'
    , encoding="ISO-8859-1"
    , error_bad_lines=False
    , usecols=['FECHA', 'CCAA', 'PCR+']
    , squeeze=True
)
# Setting the English column names
Columns = ['City', 'Date', 'New Cases']
data.columns = Columns
# Parsing columns with the correct date format
data.index = pd.to_datetime(data['Date']).dt.strftime('%d/%m/%Y')
# Setting both columns as indexes
data.set_index(['City', 'Date'], inplace=True)
# Sorting dataframe by index
data.sort_index()
# Filling NaN (Null) values
data.fillna(0.0, inplace=True)

cases = data.xs(state_name)

original, smoothed = prepare_cases(cases)

# Note that we're fixing sigma to a value just for the example
posteriors, log_likelihood = get_posteriors(smoothed, sigma=.25)

# Note that this takes a while to execute - it's not the most efficient algorithm
hdis = highest_density_interval(posteriors, p=.9)

most_likely = posteriors.idxmax().rename('ML')

# Look into why you shift -1
result = pd.concat([most_likely, hdis], axis=1)

sigmas = np.linspace(1 / 20, 1, 20)

targets = ~data.index.get_level_values('City').isin(FILTERED_REGION_CODES)
states_to_process = data.loc[targets]

results = {}

for state_name, cases in states_to_process.groupby(level='City'):

    print(state_name)
    new, smoothed = prepare_cases(cases, cutoff=25)

    if len(smoothed) == 0:
        new, smoothed = prepare_cases(cases, cutoff=10)

    result = {}

    # Holds all posteriors with every given value of sigma
    result['posteriors'] = []

    # Holds the log likelihood across all k for each value of sigma
    result['log_likelihoods'] = []

    for sigma in sigmas:
        posteriors, log_likelihood = get_posteriors(smoothed, sigma=sigma)
        result['posteriors'].append(posteriors)
        result['log_likelihoods'].append(log_likelihood)

    # Store all results keyed off of state name
    results[state_name] = result
    clear_output(wait=True)

print('Done.')

# Each index of this array holds the total of the log likelihoods for
# the corresponding index of the sigmas array.
total_log_likelihoods = np.zeros_like(sigmas)

# Loop through each state's results and add the log likelihoods to the running total.
for state_name, result in results.items():
    total_log_likelihoods += result['log_likelihoods']

# Select the index with the largest log likelihood total
max_likelihood_index = total_log_likelihoods.argmax()

# Select the value that has the highest log likelihood
sigma = sigmas[max_likelihood_index]

final_results = None

for state_name, result in results.items():
    print(state_name)
    posteriors = result['posteriors'][max_likelihood_index]
    hdis_90 = highest_density_interval(posteriors, p=.9)
    hdis_50 = highest_density_interval(posteriors, p=.5)
    most_likely = posteriors.idxmax().rename('ML')
    result = pd.concat([most_likely, hdis_90, hdis_50], axis=1)
    if final_results is None:
        final_results = result
    else:
        final_results = pd.concat([final_results, result])
    clear_output(wait=True)

# Since we now use a uniform prior, the first datapoint is pretty bogus, so just truncating it here
final_results = final_results.groupby('City').apply(lambda x: x.iloc[1:].droplevel(0))

# Uncomment the following line if you'd like to export the data
# final_results.to_csv('C:/developmentFiles/spanish_R_predicted_values.csv')
final_results = final_results.reset_index(drop= False)
dbConn.insert_dataframe_to_db(final_results, 'evaluated_r')

